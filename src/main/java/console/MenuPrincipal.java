package console;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.github.lgooddatepicker.components.DateTimePicker;

public class MenuPrincipal extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static JLabel lblMensaje;
	private static JLabel lblTitulo;
	private static JButton btnCerrar;
	private static JLabel lblFechaYHora;
	private static JLabel lblFechaYHora_1;
	private static DateTimePicker dateTimePickerInicio;
	private static DateTimePicker dateTimePickerFin;
	private static JButton btnNewButton;
	private static JButton button;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal frame = new MenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 733, 470);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null); // mandar al centro

		lblTitulo = new JLabel("Bienvenido a FarmaTurn");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblTitulo.setBounds(220, 13, 320, 39);
		contentPane.add(lblTitulo);

		lblMensaje = new JLabel(
				"<html><body>En esta pagina podra buscar farmacias de la localidad de Los Polvorines. Podra ver  <br> las que son de turno, asi tambien como visualizar farmacias abiertas en un rango especificado</body></html>");
		lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblMensaje.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblMensaje.setBounds(49, 62, 622, 78);
		contentPane.add(lblMensaje);

		btnCerrar = new JButton("Cerrar");
		btnCerrar.setBounds(593, 385, 97, 25);
		contentPane.add(btnCerrar);

		lblFechaYHora = new JLabel("Fecha y Hora de inicio");
		lblFechaYHora.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFechaYHora.setBounds(49, 241, 181, 16);
		contentPane.add(lblFechaYHora);

		lblFechaYHora_1 = new JLabel("Fecha y Hora de fin\r\n");
		lblFechaYHora_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFechaYHora_1.setBounds(49, 289, 181, 16);
		contentPane.add(lblFechaYHora_1);

		dateTimePickerInicio = new DateTimePicker();
		dateTimePickerInicio.setBounds(231, 239, 289, 23);
		contentPane.add(dateTimePickerInicio);

		dateTimePickerFin = new DateTimePicker();
		dateTimePickerFin.setBounds(231, 287, 289, 23);
		contentPane.add(dateTimePickerFin);

		btnNewButton = new JButton("Ver farmacias en general");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setBounds(59, 153, 281, 49);
		contentPane.add(btnNewButton);

		button = new JButton("Ver farmacias por rango");
		button.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button.setBounds(49, 334, 281, 49);
		contentPane.add(button);

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Abrir vista de catalogo de farmacias");
				VentanaCatalogoFarmacias vcf = new VentanaCatalogoFarmacias();
				contentPane.add(vcf);
				try {
					vcf.setMaximum(true);
				} catch (PropertyVetoException e) {
					e.printStackTrace();
				}
				vcf.moveToFront();
				vcf.setVisible(true);
				visibilidadDeMenu(false);
			}
		});
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Abrir vista de farmacias por rango");
				System.out.println(dateTimePickerInicio.datePicker.getText() + " - "
						+ dateTimePickerInicio.getTimePicker().getText());
				System.out.println(
						dateTimePickerFin.datePicker.getText() + " - " + dateTimePickerFin.getTimePicker().getText());
			}
		});
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	public static void visibilidadDeMenu(boolean b) {
		lblMensaje.setVisible(b);
		lblTitulo.setVisible(b);
		btnCerrar.setVisible(b);
		lblFechaYHora.setVisible(b);
		lblFechaYHora_1.setVisible(b);
		dateTimePickerInicio.setVisible(b);
		dateTimePickerFin.setVisible(b);
		btnNewButton.setVisible(b);
		button.setVisible(b);
	}

}
