package console;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

public class VentanaCatalogoFarmacias extends JInternalFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaCatalogoFarmacias frame = new VentanaCatalogoFarmacias();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaCatalogoFarmacias() {
		setResizable(true);
		setTitle("Catalogo de farmacias");
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);

		JPanel contentPane = new JPanel();
		contentPane.setBounds(0, 0, 434, 264);
		getContentPane().add(contentPane);
		contentPane.setLayout(null);

		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.setBounds(325, 226, 97, 25);
		contentPane.add(btnCerrar);
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				MenuPrincipal.visibilidadDeMenu(true);
			}
		});
	}
}
